// JavaScript Document

/* ************************************************************************************************************************

HoMedics

File:			app.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2019

************************************************************************************************************************ */

/* Foundation */

$(document).foundation();

/* WOW */

new WOW().init();

/* jQuery */

/*jQuery.noConflict();*/

$(document).ready(function () {
	/* Top 3 */
	var nav = $( '.top_3' );
	$( window ).scroll(function () {
		if ( $( this ).scrollTop() > 160 ) {
			nav.addClass( 'f-nav' );
		} else {
			nav.removeClass( 'f-nav' );
		}
	});
	/* Owl Carousel 2 */
	$( '.owl-carousel' ).owlCarousel({
		loop: true,
		margin: 10,
		nav: true,
		autoplay: true,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 3
			},
			1000: {
				items: 6
			}
		}
	});
});